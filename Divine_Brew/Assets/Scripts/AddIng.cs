﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddIng : MonoBehaviour
{

    public int num_correct;

    public List<string> correctIng = new List<string>();

    public int ch_num;
    public int bh_num;
    public int ci_num;
    public int cp_num;
    public int sd_num;
    public int la_num;

    public int hq_num;
    public int lq_num;


    // Use this for initialization
    void Start()
    {
        ch_num = 5;
        bh_num = 5;
        ci_num = 5;
        cp_num = 5;
        sd_num = 5;
        la_num = 5;

        hq_num = 0;
        lq_num = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    //called when corresponding button is pressed
    public void Add_ch()
    {
        if (ch_num >= 1 && GameObject.Find("_Manager").GetComponent<StockButton>().hasCh == false)
        {
            ch_num -= 1;
        }
        else if (ch_num <= 0)
        {
            ch_num = 0;
        }
    }

    public void Add_bh()
    {
        if (bh_num >= 1 && GameObject.Find("_Manager").GetComponent<StockButton>().hasBh == false)
        {
            bh_num -= 1;
        }
        else if (bh_num <= 0)
        {
            bh_num = 0;
        }
    }

    public void Add_ci()
    {
        if (ci_num >= 1 && GameObject.Find("_Manager").GetComponent<StockButton>().hasCi == false)
        {
            ci_num -= 1;
        }
        else if (ci_num <= 0)
        {
            ci_num = 0;
        }
    }

    public void Add_cp()
    {
        if (cp_num >= 1 && GameObject.Find("_Manager").GetComponent<StockButton>().hasCp == false)
        {
            cp_num -= 1;
        }
        else if (cp_num <= 0)
        {
            cp_num = 0;
        }
    }

    public void Add_sd()
    {
        if (sd_num >= 1 && GameObject.Find("_Manager").GetComponent<StockButton>().hasSd == false)
        {
            sd_num -= 1;
        }
        else if (sd_num <= 0)
        {
            sd_num = 0;
        }
    }

    public void Add_la()
    {
        if (la_num >= 1 && GameObject.Find("_Manager").GetComponent<StockButton>().hasLa == false)
        {
            la_num -= 1;
        }
        else if (la_num <= 0)
        {
            la_num = 0;
        }
    }
    
    //check if boxes are correct
    public void CheckIng()
    {
        //chamomile, cinnamon, snapdragon are correct - in List
        if (GameObject.Find("_Manager").GetComponent<StockButton>().hasCh == true)
        {
            num_correct += 1;
        }

        if (GameObject.Find("_Manager").GetComponent<StockButton>().hasCi == true)
        {
            num_correct += 1;
        }

        if (GameObject.Find("_Manager").GetComponent<StockButton>().hasSd == true)
        {
            num_correct += 1;
        }

        if (num_correct == 3)
        {
            Debug.Log("high quality");
            hq_num += 1;
        }

        if (num_correct == 2)
        {
            Debug.Log("low quality");
            lq_num += 1;
        }

        if (num_correct < 2)
        {
            Debug.Log("no potion");
        }

        //need num_correct to go back to zero but not immediately

    }

    //move ing back to stock
    public void ToStock()
    {

    }
}
