﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StockButton : MonoBehaviour {

    public Text chText;
    public Text laText;
    public Text bhText;
    public Text ciText;
    public Text cpText;
    public Text sdText;

    public Text box1Text;
    public Text box2Text;
    public Text box3Text;

    public Text history1Text;
    public Text history2Text;
    public Text history3Text;

    public Text correctText;

    public Text hqText;
    public Text lqText;

    private bool isFull1;
    private bool isFull2;
    private bool isFull3;

    public bool hasCh;
    public bool hasLa;
    public bool hasBh;
    public bool hasCi;
    public bool hasCp;
    public bool hasSd;

    // Use this for initialization
    void Start () {
        SetStockAmt();
	}
	
	// Update is called once per frame
	void Update () {
        SetStockAmt();

        correctText.text = "Correct: " + GameObject.Find("_Manager").GetComponent<AddIng>().num_correct + " / 3";

        hqText.text = "High Quality Potions: " + GameObject.Find("_Manager").GetComponent<AddIng>().hq_num;
        lqText.text = "Low Quality Potions: " + GameObject.Find("_Manager").GetComponent<AddIng>().lq_num;
    }

    //set stock button amounts
    void SetStockAmt()
    {
        chText.text = "Chamomile " + GameObject.Find("_Manager").GetComponent<AddIng>().ch_num;
        laText.text = "Lavendar " + GameObject.Find("_Manager").GetComponent<AddIng>().la_num;
        bhText.text = "Bleeding Heart " + GameObject.Find("_Manager").GetComponent<AddIng>().bh_num;
        ciText.text = "Cinnamon " + GameObject.Find("_Manager").GetComponent<AddIng>().ci_num;
        cpText.text = "Chili Pepper " + GameObject.Find("_Manager").GetComponent<AddIng>().cp_num;
        sdText.text = "Snapdragon " + GameObject.Find("_Manager").GetComponent<AddIng>().sd_num;
    }

    //put ingredients in boxes
    public void AddToPotion_ch()
    {
        if (GameObject.Find("_Manager").GetComponent<AddIng>().ch_num > 0)
        {
            GameObject.Find("_Manager").GetComponent<AddIng>().Add_ch();

            if (isFull1 == false)
            {
                box1Text.text = "Chamomile";
                isFull1 = true;
                hasCh = true;
            }
            else if (isFull2 == false)
            {
                if (box1Text.text != "Chamomile")
                {
                    box2Text.text = "Chamomile";
                    isFull2 = true;
                    hasCh = true;
                }
            }
            else if (isFull3 == false)
            {
                if (box1Text.text != "Chamomile" && box2Text.text != "Chamomile")
                {
                    box3Text.text = "Chamomile";
                    isFull3 = true;
                    hasCh = true;
                }
            }
        }
    }

    public void AddToPotion_la()
    {
        if (GameObject.Find("_Manager").GetComponent<AddIng>().la_num > 0)
        {
            GameObject.Find("_Manager").GetComponent<AddIng>().Add_la();

            if (isFull1 == false)
            {
                box1Text.text = "Lavendar";
                isFull1 = true;
                hasLa = true;
            }
            else if (isFull2 == false)
            {
                if (box1Text.text != "Lavendar")
                {
                    box2Text.text = "Lavendar";
                    isFull2 = true;
                    hasLa = true;
                }
            }
            else if (isFull3 == false)
            {
                if (box1Text.text != "Lavendar" && box2Text.text != "Lavendar")
                {
                    box3Text.text = "Lavendar";
                    isFull3 = true;
                    hasLa = true;
                }
            }
        }
    }

    public void AddToPotion_bh()
    {
        if (GameObject.Find("_Manager").GetComponent<AddIng>().bh_num > 0)
        {
            GameObject.Find("_Manager").GetComponent<AddIng>().Add_bh();

            if (isFull1 == false)
            {
                box1Text.text = "Bleeding Heart";
                isFull1 = true;
                hasBh = true;
            }
            else if (isFull2 == false)
            {
                if (box1Text.text != "Bleeding Heart")
                {
                    box2Text.text = "Bleeding Heart";
                    isFull2 = true;
                    hasBh = true;
                }
            }
            else if (isFull3 == false)
            {
                if (box1Text.text != "Bleeding Heart" && box2Text.text != "Bleeding Heart")
                {
                    box3Text.text = "Bleeding Heart";
                    isFull3 = true;
                    hasBh = true;
                }
            }
        }
    }

    public void AddToPotion_ci()
    {
        if (GameObject.Find("_Manager").GetComponent<AddIng>().ci_num > 0)
        {
            GameObject.Find("_Manager").GetComponent<AddIng>().Add_ci();

            if (isFull1 == false)
            {
                box1Text.text = "Cinnamon";
                isFull1 = true;
                hasCi = true;
            }
            else if (isFull2 == false)
            {
                if (box1Text.text != "Cinnamon")
                {
                    box2Text.text = "Cinnamon";
                    isFull2 = true;
                    hasCi = true;
                }
            }
            else if (isFull3 == false)
            {
                if (box1Text.text != "Cinnamon" && box2Text.text != "Cinnamon")
                {
                    box3Text.text = "Cinnamon";
                    isFull3 = true;
                    hasCi = true;
                }
            }
        }
    }

    public void AddToPotion_cp()
    {
        if (GameObject.Find("_Manager").GetComponent<AddIng>().cp_num > 0)
        {
            GameObject.Find("_Manager").GetComponent<AddIng>().Add_cp();

            if (isFull1 == false)
            {
                box1Text.text = "Chili Pepper";
                isFull1 = true;
                hasCp = true;
            }
            else if (isFull2 == false)
            {
                if (box1Text.text != "Chili Pepper")
                {
                    box2Text.text = "Chili Pepper";
                    isFull2 = true;
                    hasCp = true;
                }
            }
            else if (isFull3 == false)
            {
                if (box1Text.text != "Chili Pepper" && box2Text.text != "Chili Pepper")
                {
                    box3Text.text = "Chili Pepper";
                    isFull3 = true;
                    hasCp = true;
                }
            }
        }
    }

    public void AddToPotion_sd()
    {
        if (GameObject.Find("_Manager").GetComponent<AddIng>().sd_num > 0)
        {
            GameObject.Find("_Manager").GetComponent<AddIng>().Add_sd();

            if (isFull1 == false)
            {
                box1Text.text = "Snapdragon";
                isFull1 = true;
                hasSd = true;
            }
            else if (isFull2 == false)
            {
                if (box1Text.text != "Snapdragon")
                {
                    box2Text.text = "Snapdragon";
                    isFull2 = true;
                    hasSd = true;
                }
            }
            else if (isFull3 == false)
            {
                if (box1Text.text != "Snapdragon" && box2Text.text != "Snapdragon")
                {
                    box3Text.text = "Snapdragon";
                    isFull3 = true;
                    hasSd = true;
                }
            }
        }
    }

    //move boxes to history
    public void ToHistory()
    {
        history1Text.text = box1Text.text;
        history2Text.text = box2Text.text;
        history3Text.text = box3Text.text;

        box1Text.text = "";
        box2Text.text = "";
        box3Text.text = "";

        isFull1 = false;
        isFull2 = false;
        isFull3 = false;

        hasBh = false;
        hasCh = false;
        hasCi = false;
        hasCp = false;
        hasLa = false;
        hasSd = false;
    }


}